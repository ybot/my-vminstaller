#!/bin/bash
set -Eeuo pipefail
: ${MY_IPV6_RANGE_SIZE:=56}

MY_LINODE_ID=${1:-}
MY_LINODE_VLAN=${2:-}

if [ -z "${MY_LINODE_ID}" ] || [ -z "${MY_LINODE_VLAN}" ]; then
	echo "## usage:"
	echo "##    make-mygw.sh <linode-id> <vlan name>"
	echo "##"
	echo "##  linode-id: id of linode to promote/assign a v6 range to"
	echo "##  vlan name: name of vlan to create or attach"
	exit 1
fi

echo "## requesting new v6 range ..."
linode-cli networking v6-range-create --prefix_length ${MY_IPV6_RANGE_SIZE} --linode_id ${MY_LINODE_ID}
echo "## done"

echo -n "## attaching new vlan nic ..."
config_id=$(linode-cli linodes configs-list ${MY_LINODE_ID} --json | jq '.[].id')

echo ".."

linode-cli linodes config-update ${MY_LINODE_ID} ${config_id} \
	--interfaces.purpose public --interfaces.label "" \
	--interfaces.purpose vlan --interfaces.label ${MY_LINODE_VLAN} \

echo "done"
