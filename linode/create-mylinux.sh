#!/bin/bash
set -Eeuo pipefail
#shopt -s expand_aliases

: ${MY_REGION:=eu-central}
: ${MY_RELEASE:=bookworm}
: ${MY_DISK_SIZE:=20000}
: ${MY_TYPE:="g6-standard-1"}
: ${MY_PASSWD:="$(cat /dev/urandom | tr -c -d '[:alnum:]' | head -c20)"}
: ${MY_LINODE_STACKSCRIPT:="$(linode-cli stackscripts ls --label mylinux --json | jq '.[].id')"}
: ${MY_IPV6_RANGE_SIZE:=56}
: ${MY_LIN_USER:=}
: ${MY_IMAGE:="linode/debian12"}

: ${MY_HOSTNAME:=""}
: ${MY_DOMAIN:=""}
: ${MY_LABEL:=""}

uplink=""
vlan=""

function usage {
	if [ -n "${1:-}" ]; then
		cat <<-EOF
			###
			###  ${1}
			###
		EOF
	fi
	cat <<-EOF
		###
		### Usage: $0 --fqdn=<fqdn> [ --uplink ] [ --vlan=<vlan_name> ]
		###
		### --fqdn:     fqdn of linode, the hostname will be used as linode label
		###
		### --uplink:   if set turn on public/direct uplink for a router-vm
		###             this will be the first nic (eth0)
		###             default is off, unless no vlan is passed either
		###
		### --vlan:     name for vlan to attach nic to
		###             this nic will be after the uplink (eth1 if uplink present, otherwise eth0)
		###
		### if neither uplink nor vlan is defined it will default to using a public uplink and no vlan
		###
	EOF
	exit 1
}

for i in "$@"; do
	case ${i,,} in
		--fqdn=*)
			fqdn="${i#*=}"
			shift # past argument=value
			MY_HOSTNAME="${fqdn%%.*}"
			MY_DOMAIN="${fqdn#*.}"
			MY_LABEL="${MY_HOSTNAME}.${MY_DOMAIN%%.*}"
		;;
		--uplink)
			shift # past argument with no value
			uplink=true
		;;
		--vlan=*)
			vlan="${i#*=}"
			shift # past argument=value
		;;
		--help)
			usage
		;;
		*)
			usage "$i is not a valid option"
		;;
	esac
done

if [ -z "${MY_HOSTNAME}" ] || [ -z "${MY_DOMAIN}" ] || [ "${MY_HOSTNAME}" == "${MY_DOMAIN}" ]; then
	usage "fqdn not provided or not valid"
	exit 1
fi

if [ -z "${vlan}" ]; then
	uplink=true
fi

if [ -n "${uplink}" ]; then
	: ${MY_IPV6:="slaac"}
	: ${MY_IPV4:="dhcp"}
else
	: ${MY_IPV6:="dhcppd"}
	: ${MY_IPV4:="off"}
fi

echo "############################################################################"
echo "##                                                                        ##"
echo "##  YOUR PASSWORD: ${MY_PASSWD}                                   ##"
echo "##                                                                        ##"
echo "############################################################################"

echo -n "## creating linode ${MY_HOSTNAME}.${MY_DOMAIN} ..."
linode-cli linodes create --type ${MY_TYPE} --region ${MY_REGION} --image ${MY_IMAGE} --root_pass "${MY_PASSWD}" --label ${MY_LABEL} --stackscript_id ${MY_LINODE_STACKSCRIPT} --stackscript_data '{"MY_HOSTNAME": "'${MY_HOSTNAME}'", "MY_DOMAINNAME": "'${MY_DOMAIN}'", "MY_RELEASE": "'${MY_RELEASE}'", "MY_PASSWORD": "'${MY_PASSWD}'", "IPV4": "'${MY_IPV4}'", "IPV6": "'${MY_IPV6}'"}'  --booted 0 --json --no-defaults >${MY_LABEL}.json
linode_id=$(jq '.[].id' ${MY_LABEL}.json)
echo "..."
linode-cli linodes update ${linode_id} --watchdog_enabled 0

echo -n "## waiting for provision ..."
while linode-cli linodes view ${linode_id} --json | jq -e '.[].status | test("^offline$") | not' >/dev/null; do echo -n "."; sleep 1; done
echo done

echo -n "## deleting swap ..."
linode-cli linodes disk-delete ${linode_id} $(linode-cli linodes disks-list ${linode_id} --json | jq '.[] | select(.filesystem == "swap") | .id')
while linode-cli linodes disks-list ${linode_id} --json | jq -e '.[] | select(.filesystem == "swap")' >/dev/null; do echo -n "."; sleep 1; done
echo done

echo -n "## resizing temp rootdisk ..."
olddisk=$(linode-cli linodes disks-list ${linode_id} --json | jq '.[] | select(.filesystem == "ext4") | .id')
linode-cli linodes disk-resize ${linode_id} ${olddisk} --size 10000
sleep 2
while linode-cli linodes disk-view ${linode_id} ${olddisk} --json | jq -e '.[].status | test("^ready$") | not' >/dev/null; do echo -n "."; sleep 1; done
echo done

echo -n "## creating new disk ..."
newdisk=$(linode-cli linodes disk-create ${linode_id} --size ${MY_DISK_SIZE} --label debootsrap --filesystem raw --json | jq '.[].id')
sleep 1
echo -n "."
while linode-cli linodes disk-view ${linode_id} ${newdisk} --json | jq -e '.[].status | test("^ready$") | not' >/dev/null 2>&1; do echo -n "."; sleep 2; done
echo done

echo -n "## attaching new disk ..."
config_id=$(linode-cli linodes configs-list ${linode_id} --json | jq '.[].id')
echo -n "."
linode-cli linodes config-update ${linode_id} ${config_id} --devices.sda.disk_id ${olddisk} --devices.sdb.disk_id ${newdisk} >/dev/null
echo -n "."
sleep 2
echo done

echo -n "## booting up and provisioning (this may take a while) ..."
linode-cli linodes boot ${linode_id}
while linode-cli linodes view ${linode_id} --json | jq -e '.[].status | test("^offline$") | not' >/dev/null; do echo -n "."; sleep 5; done
echo done

echo -n "## deleting old temp drive ..."
linode-cli linodes disk-delete ${linode_id} ${olddisk}
echo done

echo -n "## finalizing profile ..."
linode-cli linodes config-update ${linode_id} ${config_id} --label crypt --kernel linode/direct-disk --devices.sda.disk_id ${newdisk} --helpers.updatedb_disabled 1 --helpers.distro 0 --helpers.modules_dep 0 --helpers.network 0 --helpers.devtmpfs_automount 0 ${uplink:+--interfaces.purpose public --interfaces.label ""} ${vlan:+--interfaces.purpose vlan --interfaces.label ${vlan}} >/dev/null
echo -n "..."
linode-cli linodes update ${linode_id} --watchdog_enabled 1 >/dev/null
echo done

if [ -n "${uplink}" ]; then
	echo "## requesting new v6 range ..."
	linode-cli networking v6-range-create --prefix_length ${MY_IPV6_RANGE_SIZE} --linode_id ${linode_id}
	echo "## done"
fi

echo "## booting up linode again ..."
linode-cli linodes boot ${linode_id}
linode-cli linodes view ${linode_id}

echo "##############################################################################"
echo "###                                                                        ###"
echo "###  YOUR PASSWORD: ${MY_PASSWD}                                   ###"
echo "###                                                                        ###"
echo "##############################################################################"
echo "###                                                                        ###"
echo "###  NOTE:                                                                 ###"
echo "###  dont forget to login on lish to provide the password                  ###"
echo "###  your drive is encrypted and grub won't be able to boot without login  ###"
echo "###  consider changing that password                                       ###"
echo "###                                                                        ###"
echo "##############################################################################"
echo "###                                                                        ###"
echo "###  connect to serial                                                     ###"
printf "###  %-68s  ###\n" "ssh -t ${MY_LIN_USER:-<your-linode-user>}@lish-fra1.linode.com ${MY_LABEL}"
echo "###                                                                        ###"
echo "##############################################################################"
echo "###                                                                        ###"
echo "###  lish-atl1.linode.com           US, Atlanta, GA         (us-southeast) ###"
echo "###  lish-rin1.linode.com           US, Dallas, TX          (us-central)   ###"
echo "###  lish-fnc1.linode.com           US, Fremont, CA         (us-west)      ###"
echo "###  lish-cjj1.linode.com           US, Newark, NJ          (us-east)      ###"
echo "###  lish-mum1.linode.com           Mumbai, India           (ap-west)      ###"
echo "###  lish-sin1.linode.com           Asia, Singapore, SG     (ap-south)     ###"
echo "###  lish-syd1.linode.com           Sydney, Australia       (ap-southeast) ###"
echo "###  lish-shg1.linode.com           Asia, Tokyo, JP         (ap-northeast) ###"
echo "###  lish-tor1.linode.com           Canada, Toronto, ON     (ca-central)   ###"
echo "###  lish-fra1.linode.com           Europe, Frankfurt, DE   (eu-central)   ###"
echo "###  lish-lon1.linode.com           Europe, London, UK      (eu-west)      ###"
echo "###                                                                        ###"
echo "##############################################################################"

echo "ssh -t ${MY_LIN_USER}@lish-fra1.linode.com ${MY_LABEL}"
